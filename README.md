Ansible Saxon (Java) role
=========

A role which sets up the [Saxon](https://wwww.saxonica.com) [xslt](https://www.w3.org/TR/xslt-30/) processor for use in CentOS.  The role could probably also be used in other environments by updating some default values.
The role offers support for setting up the `HE`, the `PE` or the `EE` version of the java version of Saxon.

The role primarily offers conventions for making Saxon (and downloading extensions) easily available as a CLI options, or for build systems like `ant` or when combining with other tools like xspec, by offering conventions like `SAXON_HOME` environment var.

The HE jars are downloaded by default from maven, while the PE and EE versions are downloaded by default from the saxonica.com site. 

```xml
<property environment="env"/>
<classpath>
  <fileset dir="${env.SAXON_HOME}">
    <include name="saxon*.jar"/>
    <include name="lib/*.jar"/>
  <fileset>
<classpath>
```


Requirements
------------


Role Variables
--------------
 
The settable variables (with default values) are
```yml
saxon_version: "10.5"
saxon_edition: "HE" # HE, PE or EE are valid inputs
saxon_home: "/opt/saxon"
saxon_java_install: false
saxon_java_package: "java-11-openjdk" # CentOS package name

saxon_he_url: "https://repo1.maven.org/maven2/net/sf/saxon/Saxon-HE/{{ saxon_version }}/Saxon-HE-{{ saxon_version }}.jar"
saxon_pe_url: "https://www.saxonica.com/download/SaxonPE{{ saxon_version }}J.zip"
saxon_ee_url: "https://www.saxonica.com/download/SaxonEE{{ saxon_version }}J.zip"
```

All the download urls can be overriden which is useful for self-hosting older specific PE or EE editions. The other variables would then not have to be set other than the `saxon_edition` parameter (to select the correct overriden url).

`saxon_extension_jars` is not set by default, but can be a list of urls to downloadable jars for extensions. These jars are placed in `$SAXON_HOME/lib`

Directory listing and archive for viewing saxon version names for EE and PE are found in https://www.saxonica.com/download/

Dependencies
------------
This role has  been used with CentOS 7/8 and Rocky Linux 8. Saxon uses java, but has a boolean to install an openjdk version.

Example Playbook
----------------


```yml
    - hosts: servers
      tasks:
      - import_role:
          name: "saxon"
        vars: 
          saxon_java_install: true     
```

This is the minimal role if java has not been installed by earlier roles. 

If one of `PE` or `EE` versions has been specified, `saxon_license_file` (Copy task of local file) is required.

```yml
    - hosts: servers
      tasks:
      - import_role:
          name: "saxon"
        vars:
          saxon_java_install: true
          saxon_edition: EE
          saxon_license_file: "files/saxon-license.lic" # example of earlier version
          saxon_version: "9.9.1-8"
```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
